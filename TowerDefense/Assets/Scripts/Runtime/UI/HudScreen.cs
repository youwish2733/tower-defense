using System.Collections.Generic;
using TMPro;
using TowerDefense;
using UnityEngine;
using UnityEngine.UI;

public class HudScreen : BaseScreen
{
    [SerializeField] private Button exitButton;
    [SerializeField] private Button upgradeButton;
    [SerializeField] private TextMeshProUGUI credits;
    [SerializeField] private TextMeshProUGUI lives;
    [SerializeField] private TextMeshProUGUI towerLevel;
    [SerializeField] private TextMeshProUGUI towerName;
    [SerializeField] private TowerElement towerElement;
    [SerializeField] private Transform towersContent;
    [SerializeField] private GameObject towerSection;

    private TowerDefenseModel model;
    private List<TowerElement> towers = new List<TowerElement>();

    protected override void Start()
    {
        base.Start();

        exitButton.onClick.AddListener(OnEnd);
        upgradeButton.onClick.AddListener(UpgradeTower);

        model = ViewModelManager.Get<TowerDefenseModel>();
        model.Credits.Changed += UpdateCredits;
        model.Lives.Changed += UpdateLives;
        model.TowerLevel.Changed += UpdateTowerLevel;
        model.TowerName.Changed += UpdateTowerName;

        model.Credits.Set(20);
        model.Lives.Set(10);

        towerSection.SetActive(false);

        var towerDefinitions = Game.Instance.TowerDataManager.Definitions;
        foreach (var towerDefinition in towerDefinitions)
        {
            var tower = Instantiate(towerElement, towersContent);
            tower.Setup(towerDefinition.Value);
            towers.Add(tower);
        }
    }

    private void UpdateTowersByCredits(int credits)
    {
        for (var i = 0; i < towers.Count; i++)
        {
            towers[i].UpdateByCredits(credits);
        }
    }

    private void UpdateCredits(int value)
    {
        credits.SetText(value.ToString());
        UpdateTowersByCredits(value);
        upgradeButton.interactable = value >= Constants.Game.UPGRADE_COST;
    }

    private void UpdateLives(int value)
    {
        lives.SetText(value.ToString());
    }

    private void UpdateTowerLevel(int value)
    {
        if (!towerSection.activeInHierarchy)
        {
            towerSection.SetActive(true);
        }
        towerLevel.SetText(value.ToString());
    }

    private void UpdateTowerName(string name)
    {
        if (!towerSection.activeInHierarchy)
        {
            towerSection.SetActive(true);
        }
        towerName.SetText(name);
    }

    private void UpgradeTower()
    {
        GameController.Instance.TowerManager.UpgradeTower(model.TowerId);
        model.Credits.Set(model.Credits.Value - Constants.Game.UPGRADE_COST);
        model.TowerLevel.Set(model.TowerLevel.Value + 1);
    }

    /// <summary>
    /// Ends the game.
    /// </summary>
    private void OnEnd()
    {
        var endModel = ViewModelManager.Get<EndModel>();
        endModel.GameOver = false;
        GameController.Instance.EndGameForce();
    }
}
