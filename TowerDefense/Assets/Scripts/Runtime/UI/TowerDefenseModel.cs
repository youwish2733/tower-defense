public class TowerDefenseModel : Model
{
    public Observable<int> Credits = new Observable<int>(20);
    public Observable<int> TowerLevel = new Observable<int>();
    public Observable<int> Lives = new Observable<int>(10);
    public Observable<int> EnemiesDied = new Observable<int>();
    public Observable<int> EnemiesSurvived = new Observable<int>();
    public Observable<string> TowerName = new Observable<string>();
    public string TowerId;

    /// <summary>
    /// Resets model.
    /// </summary>
    public void ClearObservables()
    {
        Credits = new Observable<int>(20);
        TowerLevel = new Observable<int>();
        Lives = new Observable<int>(10);
        EnemiesDied = new Observable<int>();
        EnemiesSurvived = new Observable<int>();
        TowerName = new Observable<string>();

        Credits.RemoveListeners();
        TowerLevel.RemoveListeners();
        Lives.RemoveListeners();
        EnemiesDied.RemoveListeners();
        TowerName.RemoveListeners();
    }
}
