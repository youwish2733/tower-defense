using TMPro;
using TowerDefense;
using UnityEngine;
using UnityEngine.UI;

public class EndScreen : BaseScreen
{
    [SerializeField] private Button backButton;
    [SerializeField] private TextMeshProUGUI titleText;

    protected override void Start()
    {
        base.Start();

        backButton.onClick.AddListener(OnBackPressed);

        var model = Game.Instance.ModelManager.Get<EndModel>();
        titleText.SetText(model.GameOver ? "Game over" : "Finished");
    }

    /// <summary>
    /// Starts the game.
    /// </summary>
    private void OnBackPressed()
    {
        ScreenManager.Show<StartScreen>();
    }
}
