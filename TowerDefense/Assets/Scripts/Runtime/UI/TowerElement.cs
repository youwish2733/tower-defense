using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TowerDefense
{
    // UI element for a tower in the HUD
    public class TowerElement : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] private Image clickable;
        [SerializeField] private Image icon;

        private Color disabledColor;
        private Color initialColor;

        private void Start()
        {
            initialColor = icon.color;
            disabledColor = icon.color;
            disabledColor.a = 0.5f;
        }

        private TowerDefinition definition;

        public void Setup(TowerDefinition definition)
        {
            this.definition = definition;
            icon.sprite = Resources.Load<Sprite>("Icons/" + definition.Icon);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            var dropArea = GameController.Instance.DropArea;
            dropArea.StartDrag(definition.Id);
            Game.Instance.CursorController?.SetCursor(icon.sprite, true);
        }

        /// <summary>
        /// Sets the tower element as interactable if player has enough credits to buy it.
        /// </summary>
        /// <param name="credits"></param>
        public void UpdateByCredits(int credits)
        {
            if (credits > definition.Cost)
            {
                clickable.raycastTarget = true;
                icon.color = initialColor;
            }
            else
            {
                clickable.raycastTarget = false;
                icon.color = disabledColor;
            }
        }
    }
}