using TowerDefense;
using UnityEngine;
using UnityEngine.UI;

public class StartScreen : BaseScreen
{
    [SerializeField] private Button startButton;

    protected override void Start()
    {
        base.Start();

        startButton.onClick.AddListener(StartGame);
    }

    /// <summary>
    /// Starts the game.
    /// </summary>
    private void StartGame()
    {
        GameController.Instance.StartGame();
    }
}
