using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefense
{
    public class TowerDraggable : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        [SerializeField] private RectTransform rectTransform;
        [SerializeField] private Image image;

        public string Id { get; private set; }

        public void Setup(string id, Sprite sprite)
        {
            Id = id;
            image.sprite = sprite;
            transform.SetAsLastSibling();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
        }

        public void OnDrag(PointerEventData eventData)
        {
            rectTransform.anchoredPosition += eventData.delta;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
        }
    }
}