using UnityEngine;

namespace TowerDefense
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance
        {
            get => instance;
        }

        private static UIManager instance = null;

        private ScreenManager screenManager => ScreenManager.Instance;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        private void Start()
        {
            screenManager.RegisterScreens();
            screenManager.StartWith<StartScreen>();
        }
    }
}