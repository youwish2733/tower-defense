using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TowerDefense
{
    public class Game : MonoBehaviour
    {
        public static Game Instance
        {
            get => instance;
        }

        private static Game instance = null;

        public ModelManager ModelManager => ModelManager.Instance;
        public ScreenManager ScreenManager => ScreenManager.Instance;
        public TowerDataManager TowerDataManager;
        public EnemyDataManager EnemyDataManager;
        public LevelDataManager LevelDataManager;
        public CursorController CursorController;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        /// <summary>
        /// Load game scene when game starts.
        /// </summary>
        public void StartGame()
        {
            SceneManager.LoadScene("GameScene");
        }

        /// <summary>
        /// Load end scene when game ends with a given delay.
        /// </summary>
        /// <param name="delay"></param>
        public void EndGame(float delay = 1f)
        {
            StartCoroutine(EndWithDelay(delay));
        }

        /// <summary>
        /// Load end scene when game ends without a delay.
        /// </summary>
        /// <param name="delay"></param>
        public void EndGameForce()
        {
            End();
        }

        private IEnumerator EndWithDelay(float delay)
        {
            yield return new WaitForSeconds(delay);
            End();
        }

        private void End()
        {
            SceneManager.LoadScene("MenuScene");
            SceneManager.sceneLoaded += MenuSceneLoaded;
        }

        private void MenuSceneLoaded(Scene _, LoadSceneMode __)
        {
            SceneManager.sceneLoaded -= MenuSceneLoaded;
            ScreenManager.RegisterScreens();
            ScreenManager.Show<EndScreen>();
        }
    }
}