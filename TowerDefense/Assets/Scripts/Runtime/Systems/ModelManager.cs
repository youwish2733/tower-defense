using System;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense
{
    // Model manager for all models
    public class ModelManager : MonoBehaviour
    {
        // Singleton for the ModelManager class
        public static ModelManager Instance
        {
            get
            {
                if (instance == null)
                {
                    if (instance == null)
                    {
                        var go = new GameObject();
                        go.name = "ModelRepository";
                        instance = go.AddComponent<ModelManager>();

                        DontDestroyOnLoad(go);
                    }
                }

                return instance;
            }
        }
        private static ModelManager instance = null;

        private Dictionary<Type, Model> models { get; set; } = new Dictionary<Type, Model>();

        /// <summary>
        /// Returns the model associated with the specified type or adds a new one if it doesn't exist.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Get<T>() where T : Model, new()
        {
            if (models.ContainsKey(typeof(T)))
            {
                return models[typeof(T)] as T;
            }
            var vm = new T();
            models.Add(typeof(T), vm);
            return vm;
        }
    }
}