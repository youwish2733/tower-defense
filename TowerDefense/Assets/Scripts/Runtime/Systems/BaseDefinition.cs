using System;

[Serializable]
public class BaseDefinition
{
    public string Id;
    public string Name;
}
