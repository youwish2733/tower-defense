using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// Data driven approach to load definitions.
[UnityEngine.Scripting.Preserve]
public class DataManager<T> : MonoBehaviour where T : BaseDefinition
{
    [Serializable]
    public class Data
    {
        public List<T> Items = new List<T>();
    }

    [SerializeField]
    private TextAsset dataJson;

    public Dictionary<string, T> Definitions = new Dictionary<string, T>();
    
    protected virtual void Awake()
    {

    }

    protected virtual void Start()
    {
        LoadDefinitions();
    }

    protected virtual void Update()
    {

    }

    /// <summary>
    /// Loads definitions from json file.
    /// </summary>
    protected void LoadDefinitions()
    {
        if (dataJson == null)
        {
            Debug.LogError("Json file not found. Cannot load definitions!");
            return;
        }

        var data = JsonUtility.FromJson<Data>(dataJson.text);
        Definitions = data.Items.ToDictionary(d => d.Id, d => d);
    }

    /// <summary>
    /// Returns a definition by id.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public T GetDefinition(string id)
    {
        return Definitions.ContainsKey(id) ? Definitions[id] : null;
    }
}
