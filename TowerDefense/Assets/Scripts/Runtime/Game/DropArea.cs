using UnityEngine;
using UnityEngine.Tilemaps;

namespace TowerDefense
{
    // The area where Towers are placed.
    public class DropArea : MonoBehaviour
    {
        [SerializeField] private Tilemap solidMap;
        [SerializeField] private Tilemap hoverMap;

        private bool isEnabled;
        private string draggedId;

        private bool[,] occupiedTiles;
        private Color freeColor;
        private Color occupiedColor;
        private Tile hoverTile;
        private Sprite hoverSprite;
        private Vector3Int previousHighlightTilePosition;

        private void Start()
        {
            GameController.Instance.SetDropArea(this);

            var bounds = solidMap.cellBounds;

            // Initialize the occupiedTiles array with the same dimensions as the solidMap
            occupiedTiles = new bool[bounds.size.x, bounds.size.y];

            ColorUtility.TryParseHtmlString(Constants.Colors.FREE_TILE, out freeColor);
            ColorUtility.TryParseHtmlString(Constants.Colors.OCCUPIED_TILE, out occupiedColor);
            occupiedColor.a = 0.85f;
            freeColor.a = 0.85f;

            var pixelsPerUnity = (int)solidMap.GetSprite(new Vector3Int(0, 0)).pixelsPerUnit;

            hoverTile = ScriptableObject.CreateInstance<Tile>();
            hoverSprite = Sprite.Create(new Texture2D(pixelsPerUnity, pixelsPerUnity), new Rect(0, 0, pixelsPerUnity, pixelsPerUnity), Vector2.one * 0.5f, pixelsPerUnity);
            hoverTile.sprite = hoverSprite;
        }

        private void Update()
        {
            if (!isEnabled)
            {
                return;
            }

            // Calculate the tile position in the tilemap at mouse position
            var cellPosition = solidMap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            // Apply an offset
            cellPosition.y--;

            // If mouse is released, try to drop the tower
            if (Input.GetMouseButtonUp(0))
            {
                OnDrop(cellPosition);
                return;
            }

            // Create a highlight effect on the tile at mouse position
            if (previousHighlightTilePosition == cellPosition)
            {
                return;
            }
            else
            {
                hoverMap.SetTile(previousHighlightTilePosition, null);
            }

            HighlightTile(cellPosition);
            previousHighlightTilePosition = cellPosition;
        }

        /// <summary>
        /// Enables the area to receive "drag events".
        /// </summary>
        /// <param name="id"></param>
        public void StartDrag(string id)
        {
            isEnabled = true;
            draggedId = id;
        }

        /// <summary>
        /// Executes the drop event.
        /// </summary>
        /// <param name="cellPosition"></param>
        public void OnDrop(Vector3Int cellPosition)
        {
            if (!string.IsNullOrEmpty(draggedId))
            {
                Drop(cellPosition);
            }
        }

        /// <summary>
        /// Spawns tower if tile is free.
        /// </summary>
        /// <param name="position"></param>
        private void Drop(Vector3Int position)
        {
            isEnabled = false;

            hoverMap.SetTile(position, null);
            if (solidMap.GetTile(position) != null && !IsTileOccupied(position))
            {
                MarkTileAsOccupied(position);

                // Snap position to tile
                var centeredPosition = solidMap.GetCellCenterWorld(position);

                // Spawn Tower
                GameController.Instance.TowerManager.Spawn(draggedId, centeredPosition);
            }
        }

        /// <summary>
        /// Marks tile at given position as occupied.
        /// </summary>
        /// <param name="position"></param>
        private void MarkTileAsOccupied(Vector3Int position)
        {
            // Convert the tile position to an index in the occupiedTiles array
            var x = position.x - solidMap.cellBounds.xMin;
            var y = position.y - solidMap.cellBounds.yMin;

            // Mark the tile as occupied
            occupiedTiles[x, y] = true;
        }

        /// <summary>
        /// Returns whether the tile at given position is occupied.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        private bool IsTileOccupied(Vector3Int position)
        {
            // Convert the tile position to an index in the occupiedTiles array
            var x = position.x - solidMap.cellBounds.xMin;
            var y = position.y - solidMap.cellBounds.yMin;

            // Return whether the tile has been occupied
            return occupiedTiles[x, y];
        }

        /// <summary>
        /// Applies a highlight effect on a tile at given position. If the tile is free, the highlight is green, otherwise red.
        /// </summary>
        /// <param name="cellPosition"></param>
        private void HighlightTile(Vector3Int cellPosition)
        {
            if (solidMap.GetTile(cellPosition) == null || IsTileOccupied(cellPosition))
            {
                hoverTile.color = occupiedColor;
            }
            else
            {
                hoverTile.color = freeColor;
            }

            hoverMap.SetTile(cellPosition, hoverTile);
        }
    }
}