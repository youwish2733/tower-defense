using TowerDefense;
using UnityEngine;

public class Freeze : Attack
{
    private float speed = 35f;
    private float lifetime = 4f;
    private LayerMask hitLayers;
    private float spawnTime;
    private float duration;

    private void Start()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Initializes Freeze attack.
    /// </summary>
    /// <param name="position"></param>
    /// <param name="direction"></param>
    /// <param name="duration"></param>
    /// <param name="enemyLayer"></param>
    public void Setup(Vector2 position, Vector3 direction, float duration, LayerMask enemyLayer)
    {
        transform.position = position;
        spawnTime = Time.time;
        transform.up = direction.normalized;
        hitLayers = enemyLayer;
        this.duration = duration;
    }

    private void Update()
    {
        transform.position += transform.up * speed * Time.deltaTime;

        if (Time.time > spawnTime + lifetime)
        {
            gameObject.SetActive(false);
            spawnTime = Time.time;
        }
    }

    /// <summary>
    /// Freezes an enemy if projectiles hits enemy.
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var obj = collision.gameObject;
        if ((hitLayers.value & 1 << obj.layer) != 0)
        {
            var enemy = obj.GetComponent<EnemyController>();
            if (enemy)
            {
                enemy.Freeze(duration);
            }
        }
    }

    /// <summary>
    /// Fires a Freeze projectile
    /// </summary>
    public override void Fire()
    {
        gameObject.SetActive(true);
    }
}
