using UnityEngine;

namespace TowerDefense
{
    // Projectile attack
    public class Projectile : Attack
    {
        private float speed = 25f;
        private float lifetime = 4f;
        private LayerMask hitLayers;
        private float spawnTime;
        private int damage;

        private void Start()
        {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Initializes the projectile attack.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="direction"></param>
        /// <param name="damage"></param>
        /// <param name="enemyLayer"></param>
        public void Setup(Vector2 position, Vector3 direction, int damage, LayerMask enemyLayer)
        {
            transform.position = position;
            spawnTime = Time.time;
            transform.right = direction.normalized;
            hitLayers = enemyLayer;
            this.damage = damage;
        }

        private void Update()
        {
            transform.position += transform.right * speed * Time.deltaTime;

            if (Time.time > spawnTime + lifetime)
            {
                gameObject.SetActive(false);
                spawnTime = Time.time;
            }
        }

        /// <summary>
        /// Damage enemy if projectile hits enemy.
        /// </summary>
        /// <param name="collision"></param>
        private void OnTriggerEnter2D(Collider2D collision)
        {
            var obj = collision.gameObject;
            if ((hitLayers.value & 1 << obj.layer) != 0)
            {
                gameObject.SetActive(false);

                var enemy = obj.GetComponent<EnemyController>();
                if (enemy)
                {
                    enemy.TakeDamage(damage);
                }
            }
        }

        /// <summary>
        /// Fire projectile.
        /// </summary>
        public override void Fire()
        {
            gameObject.SetActive(true);
        }
    }
}
