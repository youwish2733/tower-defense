
using UnityEngine;

/// <summary>
/// Abstract class for tower attacks
/// </summary>
public abstract class Attack : MonoBehaviour
{
    public abstract void Fire();
}
