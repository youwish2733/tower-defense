using UnityEngine;

namespace TowerDefense
{
    public class GameController
    {
        private TowerDefenseModel model;

        public GameController()
        {
            model = Game.Instance.ModelManager.Get<TowerDefenseModel>();
            var endModel = Game.Instance.ModelManager.Get<EndModel>();
            endModel.GameOver = false;
        }

        private static GameController instance = null;

        public static GameController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameController();
                }
                return instance;
            }
        }

        public DropArea DropArea { get; private set; }
        public LevelController LevelController { get; private set; }
        public TowerManager TowerManager { get; private set; }

        /// <summary>
        /// Initializes the drop area.
        /// </summary>
        /// <param name="dropArea"></param>
        public void SetDropArea(DropArea dropArea)
        {
            DropArea = dropArea;
        }

        /// <summary>
        /// Initializes the level controller.
        /// </summary>
        /// <param name="levelController"></param>
        public void SetLevelController(LevelController levelController)
        {
            LevelController = levelController;
        }

        /// <summary>
        /// Initializes the tower manager.
        /// </summary>
        /// <param name="towerManager"></param>
        public void SetTowerManager(TowerManager towerManager)
        {
            TowerManager = towerManager;
        }

        /// <summary>
        /// Sets level configuration by id.
        /// </summary>
        /// <param name="levelId"></param>
        public void SetLevel(string levelId)
        {
            if (LevelController == null)
            {
                Debug.LogError("LevelController is null!");
                return;
            }
            LevelController.Setup(levelId);
        }

        /// <summary>
        /// Starts the game.
        /// </summary>
        public void StartGame()
        {
            Game.Instance.StartGame();
        }

        /// <summary>
        /// Ends the game.
        /// </summary>
        public void EndGame()
        {
            model.ClearObservables();
            Game.Instance.EndGame();
        }


        /// <summary>
        /// Ends the game by force.
        /// </summary>
        public void EndGameForce()
        {
            model.ClearObservables();
            Game.Instance.EndGameForce();
        }
    }
}