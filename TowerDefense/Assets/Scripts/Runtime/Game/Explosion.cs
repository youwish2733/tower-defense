using DG.Tweening;
using TowerDefense;
using UnityEngine;

// Explosion attack
public class Explosion : Attack
{
    [SerializeField] private SpriteRenderer sprite;

    private float initialSize;
    private float range;
    private int damage;
    private LayerMask enemyLayer;

    private void Awake()
    {
        initialSize = sprite.bounds.size.x;
    }

    private void Start()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Initializes Explosion attack.
    /// </summary>
    /// <param name="position"></param>
    /// <param name="range"></param>
    /// <param name="damage"></param>
    /// <param name="enemyLayer"></param>
    public void Setup(Vector2 position, float range, int damage, LayerMask enemyLayer)
    {
        transform.position = position;
        this.range = range;
        this.damage = damage;
        this.enemyLayer = enemyLayer;
        var scale = (range * 2) / initialSize;
        transform.localScale = new Vector3(scale, scale, 1.0f);
    }

    /// <summary>
    /// Fires an explosion.
    /// </summary>
    public override void Fire()
    {
        // Find all enemy units within the attack range
        var colliders = Physics2D.OverlapCircleAll(transform.position, range, enemyLayer);

        // Loop through all the enemy units within the attack range
        foreach (var collider in colliders)
        {
            // Get the enemy unit's health component (or a script that handles health)
            var enemyController = collider.GetComponent<EnemyController>();

            // If the enemy unit has a health component, deal damage to it
            if (enemyController != null)
            {
                enemyController.TakeDamage(damage);
            }
        }

        // Play AOE attack animation
        gameObject.SetActive(true);
        sprite.DOKill(true);
        sprite.DOFade(0f, 0.5f)
            .OnComplete(() =>
            {
                gameObject.SetActive(false);
                sprite.DOFade(1f, 0f);
            });
    }
}
