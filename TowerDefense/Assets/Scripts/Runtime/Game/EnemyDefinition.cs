using System;

[Serializable]
public class EnemyDefinition : BaseDefinition
{
    public string Icon;
    public int HitPoints;
    public int Reward;
    public int Speed;
}
