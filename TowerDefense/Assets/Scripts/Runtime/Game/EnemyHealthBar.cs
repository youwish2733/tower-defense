using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthBar : MonoBehaviour
{
    private const float Y_OFFSET = 2.1f;

    [SerializeField] private Image healthBarImage;
    [SerializeField] private Transform enemyTransform;

    [HideInInspector] public float MaxHealth;

    private float currentHealth;

    private void Start()
    {
        currentHealth = MaxHealth;
    }

    private void Update()
    {
        var enemyPosition = enemyTransform.position;
        enemyPosition.y += Y_OFFSET;
        transform.position = enemyPosition;
    }

    /// <summary>
    /// Set health bar max health.
    /// </summary>
    /// <param name="value"></param>
    public void SetMaxHealth(float value)
    {
        MaxHealth = value;
        currentHealth = MaxHealth;
    }

    /// <summary>
    /// Apply damage taken to the health bar.
    /// </summary>
    /// <param name="damage"></param>
    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        healthBarImage.fillAmount = currentHealth / MaxHealth;
    }
}