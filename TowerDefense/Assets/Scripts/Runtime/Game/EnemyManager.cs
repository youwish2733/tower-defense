using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense
{
    public class EnemyManager : MonoBehaviour
    {
        [SerializeField] private EnemyController enemyPrefab;

        // The canvas that contains all enemy UI (health bars)
        [SerializeField] private Canvas worldCanvas;

        /// <summary>
        /// Spawns and initializes enemy at position.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="waypoints"></param>
        /// <param name="enemyId"></param>
        public void SpawnEnemy(Vector2 position, List<Transform> waypoints, string enemyId)
        {
            var enemyDefinition = Game.Instance.EnemyDataManager.GetDefinition(enemyId);
            var enemy = Instantiate(enemyPrefab, position, Quaternion.identity, transform);
            enemy.Setup(enemyDefinition, waypoints);
            enemy.SetupHealthBar(worldCanvas);
        }
    }
}