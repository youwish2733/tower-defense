using System;
using System.Collections.Generic;
using TowerDefense;
using UnityEngine;

public class TowerManager : MonoBehaviour
{
    [SerializeField] private Transform towerContainer;
    [SerializeField] private TowerController towerPrefab;

    private TowerDefenseModel model;
    private Dictionary<string, TowerController> towers = new Dictionary<string, TowerController>();

    private void Start()
    {
        model = Game.Instance.ModelManager.Get<TowerDefenseModel>();
        GameController.Instance.SetTowerManager(this);
    }

    /// <summary>
    /// Spawns tower at given position by id.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="position"></param>
    public void Spawn(string id, Vector2 position)
    {
        var definition = Game.Instance.TowerDataManager.GetDefinition(id);

        var newId = Guid.NewGuid().ToString();
        model.Credits.Set(model.Credits.Value - definition.Cost);
        var newTower = Instantiate(towerPrefab, towerContainer);
        newTower.transform.position = position;
        newTower.Setup(definition, newId);

        towers.Add(newId, newTower);
    }

    /// <summary>
    /// Upgrades tower by id.
    /// </summary>
    /// <param name="id"></param>
    public void UpgradeTower(string id)
    {
        if (towers.ContainsKey(id))
        {
            towers[id].Upgrade();
        }
    }
}
