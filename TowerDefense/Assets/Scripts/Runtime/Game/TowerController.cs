using UnityEngine;

namespace TowerDefense
{
    public class TowerController : MonoBehaviour
    {
        private const float ENEMY_POSITION_OFFSET = 1f;

        [SerializeField] private SpriteRenderer icon;
        [SerializeField] private Transform firePoint;
        [SerializeField] private SpriteRenderer rangeSprite;
        [SerializeField] private LayerMask enemyLayer;

        [HideInInspector] public string Guid;

        private TowerDefenseModel model;
        private TowerDefinition definition;
        private float cooldown = 1f;
        private float lastTimeFired = 0.0f;
        private float range = 0.0f;
        private float initialSize;
        private int damage;
        private int level;
        private Attack attack;

        private void Awake()
        {
            initialSize = rangeSprite.bounds.size.x;
        }

        private void Start()
        {
            model = Game.Instance.ModelManager.Get<TowerDefenseModel>();
        }

        /// <summary>
        /// Initializes the tower.
        /// </summary>
        /// <param name="definition"></param>
        /// <param name="guid"></param>
        public void Setup(TowerDefinition definition, string guid)
        {
            this.definition = definition;
            Guid = guid;

            range = definition.Range;
            var scale = (range * 2) / initialSize;
            rangeSprite.transform.localScale = new Vector3(scale, scale, 1.0f);
            rangeSprite.enabled = false;

            icon.sprite = Resources.Load<Sprite>("Icons/" + definition.Icon);
            cooldown = definition.Cooldown;
            lastTimeFired = definition.Cooldown;
            level = 1;
            damage = definition.Damage;
            SetAttack();
        }

        private void Update()
        {
            // Fire when off cooldown
            if (Time.time - lastTimeFired > cooldown)
            {
                // Find the closest enemy to the tower
                var closestEnemy = FindClosestEnemy();
                if (closestEnemy == null)
                {
                    return;
                }

                Fire(closestEnemy);
                lastTimeFired = Time.time;
            }
        }

        private void OnMouseEnter()
        {
            rangeSprite.enabled = true;
        }

        private void OnMouseExit()
        {
            rangeSprite.enabled = false;
        }

        /// <summary>
        /// Returns the transform of the closest enemy.
        /// </summary>
        /// <returns></returns>
        private Transform FindClosestEnemy()
        {
            // Find the closest enemy to the tower using an OverlapSphere
            var colliders = Physics2D.OverlapCircleAll(transform.position, range, enemyLayer);
            var collidersCount = colliders.Length;

            if (collidersCount == 0)
            {
                return null;
            }

            var closestColliderIndex = 0;

            // Optimized way to calculate distance.
            var closestDistance = (transform.position - colliders[closestColliderIndex].transform.position).sqrMagnitude;

            for (var i = 1; i < collidersCount; i++)
            {
                var distance = (transform.position - colliders[i].transform.position).sqrMagnitude;

                if (distance < closestDistance)
                {
                    closestColliderIndex = i;
                    closestDistance = distance;
                }
            }

            return colliders[closestColliderIndex].transform;
        }

        /// <summary>
        /// Fires tower attack at enemy.
        /// </summary>
        private void Fire(Transform enemy)
        {
            switch (definition.Type)
            {
                case "rocket":
                    SetupRocket(enemy);
                    break;
                case "explosion":
                    SetupExplosion();
                    break;
                case "freeze":
                    SetupFreeze(enemy);
                    break;
                default:
                    SetupRocket(enemy);
                    break;
            }

            attack.Fire();
        }

        /// <summary>
        /// Initializes rocket attack.
        /// </summary>
        /// <param name="closestEnemy"></param>
        private void SetupRocket(Transform closestEnemy)
        {
            // Calculate the direction to the closest enemy
            var enemyPosition = closestEnemy.position;
            enemyPosition.y += ENEMY_POSITION_OFFSET;
            var direction = enemyPosition - firePoint.position;

            var towerProjectile = (Projectile)attack;
            if (towerProjectile)
            {
                towerProjectile.Setup(firePoint.position, direction, damage, enemyLayer);
            }
        }

        /// <summary>
        /// Initializes explosion attack.
        /// </summary>
        private void SetupExplosion()
        {
            var towerExplosion = (Explosion)attack;
            if (towerExplosion)
            {
                towerExplosion.Setup(transform.position, range, damage, enemyLayer);
            }
        }

        /// <summary>
        /// Initializes freeze attack.
        /// </summary>
        /// <param name="closestEnemy"></param>
        private void SetupFreeze(Transform closestEnemy)
        {
            // Calculate the direction to the closest enemy
            var enemyPosition = closestEnemy.position;
            enemyPosition.y += ENEMY_POSITION_OFFSET;
            var direction = enemyPosition - firePoint.position;

            var towerProjectile = (Freeze)attack;
            if (towerProjectile)
            {
                towerProjectile.Setup(firePoint.position, direction, damage, enemyLayer);
            }
        }

        /// <summary>
        /// Instantiates attack projectile/aoe sprite for later use.
        /// </summary>
        private void SetAttack()
        {
            switch (definition.Type)
            {
                case "rocket":
                    var projectilePrefab = Resources.Load<Attack>("Objects/Projectile");
                    attack = Instantiate(projectilePrefab, firePoint.position, Quaternion.identity);
                    break;
                case "explosion":
                    var explosionPrefab = Resources.Load<Attack>("Objects/Explosion");
                    attack = Instantiate(explosionPrefab, firePoint.position, Quaternion.identity);
                    break;
                case "freeze":
                    var freezePrefab = Resources.Load<Attack>("Objects/Freeze");
                    attack = Instantiate(freezePrefab, firePoint.position, Quaternion.identity);
                    break;
                default:
                    var defaultPrefab = Resources.Load<Attack>("Objects/Projectile");
                    attack = Instantiate(defaultPrefab, firePoint.position, Quaternion.identity);
                    break;
            }
        }

        /// <summary>
        /// Upgrades tower.
        /// </summary>
        public void Upgrade()
        {
            level += 1;
            range += 1;
            var scale = (range * 2) / initialSize;
            rangeSprite.transform.localScale = new Vector3(scale, scale, 1.0f);

            damage += 1;
        }

        /// <summary>
        /// Selects the tower.
        /// </summary>
        private void OnMouseDown()
        {
            model.TowerName.Set(definition.Name);
            model.TowerLevel.Set(level);
            model.TowerId = Guid;
        }
    }
}