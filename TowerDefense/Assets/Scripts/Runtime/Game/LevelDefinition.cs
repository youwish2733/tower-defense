using System;
using System.Collections.Generic;

[Serializable]
public class LevelDefinition : BaseDefinition
{
    public List<EnemyAmount> Enemies = new List<EnemyAmount>();

    [Serializable]
    public struct EnemyAmount
    {
        public string EnemyId;
        public int Amount;
    }
}