using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private SpriteRenderer affectedSprite;
        [SerializeField] private SpriteRenderer damagedSprite;
        [SerializeField] private Rigidbody2D rb;
        [SerializeField] private EnemyHealthBar healthBar;

        private EnemyDefinition definition;
        private List<Transform> waypoints;

        private TowerDefenseModel model;
        private float speed;
        private float freezeTime;
        private float elapsedFreezeTime;
        private int hitPoints;
        private int waypointIndex;
        private bool isFrozen;

        private void Start()
        {
            model = Game.Instance.ModelManager.Get<TowerDefenseModel>();

            affectedSprite.enabled = false;
            damagedSprite.enabled = false;

            affectedSprite.DOFade(0f, 0f);
        }

        /// <summary>
        /// Initializes enemy.
        /// </summary>
        /// <param name="definition"></param>
        /// <param name="waypoints"></param>
        public void Setup(EnemyDefinition definition, List<Transform> waypoints)
        {
            this.definition = definition;
            this.waypoints = waypoints;

            speed = definition.Speed;
            hitPoints = definition.HitPoints;
            spriteRenderer.sprite = Resources.Load<Sprite>("Icons/" + definition.Icon);
            healthBar.SetMaxHealth(definition.HitPoints);
        }

        /// <summary>
        /// Initializes enemy health bar.
        /// </summary>
        /// <param name="canvas"></param>
        public void SetupHealthBar(Canvas canvas)
        {
            healthBar.transform.SetParent(canvas.transform);
        }

        private void Update()
        {
            if (isFrozen)
            {
                if (elapsedFreezeTime >= freezeTime)
                {
                    // Re-enable the Rigidbody to unfreeze the object
                    isFrozen = false;
                    rb.isKinematic = false;
                    affectedSprite.enabled = false;
                    affectedSprite.DOFade(0f, 0f);
                }
                else
                {
                    elapsedFreezeTime += Time.deltaTime;
                }
                return;
            }

            // Moves the enemy toward the next waypoint
            transform.position = Vector2.MoveTowards(transform.position, waypoints[waypointIndex].position, speed * Time.deltaTime);

            // If close enough, set the next waypoint for the enemy
            if (Vector2.Distance(transform.position, waypoints[waypointIndex].position) < 0.01f)
            {
                if (waypointIndex < waypoints.Count - 1)
                {
                    waypointIndex++;
                }
                else
                {
                    Survive();
                }
            }
        }

        /// <summary>
        /// Applies a given damage amount to enemy.
        /// </summary>
        /// <param name="damage"></param>
        public void TakeDamage(int damage)
        {
            if (hitPoints <= damage)
            {
                Die();
            }
            else
            {
                damagedSprite.enabled = true;
                damagedSprite.DOKill(true);
                damagedSprite.DOFade(0f, 0.2f)
                    .OnComplete(() =>
                    {
                        damagedSprite.DOFade(1f, 0f);
                        damagedSprite.enabled = false;
                    });
                hitPoints -= damage;
                healthBar.TakeDamage(damage);
            }
        }

        /// <summary>
        /// Freezes the enemy for a given duration.
        /// </summary>
        /// <param name="duration"></param>
        public void Freeze(float duration)
        {
            // Don't freeze an already frozen enemy
            if (isFrozen)
                return;
            // Set the freeze time and start the freeze countdown
            freezeTime = duration;
            elapsedFreezeTime = 0f;

            // Disable the Rigidbody to freeze the object
            isFrozen = true;
            rb.isKinematic = true;

            affectedSprite.enabled = true;
            affectedSprite.DOKill(true);
            affectedSprite.DOFade(1f, 0.25f);
        }

        /// <summary>
        /// Executes enemy death instructions.
        /// </summary>
        public void Die()
        {
            Debug.Log("Enemy died.");
            model.Credits.Set(model.Credits.Value + definition.Reward);
            model.EnemiesDied.Set(model.EnemiesDied.Value + 1);
            damagedSprite.DOKill();
            Destroy(gameObject);
            Destroy(healthBar.gameObject);
        }

        /// <summary>
        /// Executes enemy survived instructions.
        /// </summary>
        public void Survive()
        {
            model.EnemiesSurvived.Set(model.EnemiesSurvived.Value + 1);
            model.Lives.Set(model.Lives.Value - 1);
            if (model.Lives.Value <= 0)
            {
                var endModel = Game.Instance.ModelManager.Get<EndModel>();
                endModel.GameOver = true;
                GameController.Instance.EndGame();
                return;
            }

            damagedSprite.DOKill();
            Destroy(gameObject);
            Destroy(healthBar.gameObject);
        }
    }
}