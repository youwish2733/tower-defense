using System.Collections.Generic;
using TowerDefense;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    private const float MIN_SPAWN_INTERVAL = 0.4f;
    private const float MAX_SPAWN_INTERVAL = 2f;

    [SerializeField] private Transform waypointContainer;
    [SerializeField] private Transform spawnPoint;
    [SerializeField] private EnemyManager enemyManager;

    public List<Transform> Waypoints = new List<Transform>();
    public float spawnInterval;  // the interval between enemy spawns
    private float spawnTimer = 0f;  // a timer to track when to spawn the next enemy
    private LevelDefinition currentLevelDefinition;
    private TowerDefenseModel model;
    private int enemyIndex;
    private int enemyCount;
    private int totalEnemies;

    private void Start()
    {
        GameController.Instance.SetLevelController(this);

        // Hardcoding the level here, this can be automated
        Setup("level_1");

        foreach (Transform waypoint in waypointContainer)
        {
            Waypoints.Add(waypoint);
        }

        model = Game.Instance.ModelManager.Get<TowerDefenseModel>();
        model.EnemiesDied.Changed += OnEnemyDied;
        model.EnemiesSurvived.Changed += OnEnemySurvived;
    }

    /// <summary>
    /// Initializes the level controller.
    /// </summary>
    /// <param name="levelId"></param>
    public void Setup(string levelId)
    {
        totalEnemies = 0;
        currentLevelDefinition = Game.Instance.LevelDataManager.GetDefinition(levelId);

        var enemies = currentLevelDefinition.Enemies;
        for (var i = 0; i < enemies.Count; i++)
        {
            totalEnemies += enemies[i].Amount;
        }
    }

    private void Update()
    {
        spawnTimer += Time.deltaTime;

        // If the timer has reached the spawn interval, spawn an enemy and reset the timer
        if (spawnTimer >= spawnInterval)
        {
            if (currentLevelDefinition == null || enemyIndex >= currentLevelDefinition.Enemies.Count)
            {
                return;
            }

            enemyManager.SpawnEnemy(spawnPoint.position, Waypoints, currentLevelDefinition.Enemies[enemyIndex].EnemyId);

            // Move to next batch of enemies if current batch is spawned
            if (enemyCount == currentLevelDefinition.Enemies[enemyIndex].Amount - 1)
            {
                enemyIndex++;
                enemyCount = 0;
            }
            else
            {
                enemyCount++;
            }

            // Randomize spawn interval time
            var random = Random.Range(MIN_SPAWN_INTERVAL, MAX_SPAWN_INTERVAL);
            spawnInterval = random;
            spawnTimer = 0f;
        }
    }

    /// <summary>
    /// Executes on enemy died event.
    /// </summary>
    /// <param name="enemiesDied"></param>
    private void OnEnemyDied(int enemiesDied)
    {
        if (enemiesDied >= totalEnemies)
        {
            GameController.Instance.EndGame();
        }
    }

    /// <summary>
    /// Executes on enemy survived event.
    /// </summary>
    /// <param name="enemiesSurvived"></param>
    private void OnEnemySurvived(int enemiesSurvived)
    {
        if (enemiesSurvived < model.Lives.Value)
        {
            model.EnemiesDied.Set(model.EnemiesDied.Value + 1);
        }
    }
}
