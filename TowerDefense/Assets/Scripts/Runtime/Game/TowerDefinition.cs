using System;

[Serializable]
public class TowerDefinition : BaseDefinition
{
    public int Cost;
    public int Damage;
    public float Cooldown;
    public float Range;
    public string Type;
    public string Icon;
}
