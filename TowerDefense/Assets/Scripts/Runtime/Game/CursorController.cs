using TowerDefense;
using UnityEngine;
using UnityEngine.UI;

public class CursorController : MonoBehaviour
{
    [SerializeField] private Image icon;
    private bool isActivated;
    private bool clearAfterClick;

    private void Awake()
    {
        Game.Instance.CursorController = this;
        icon.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (isActivated)
        {
            icon.transform.position = Input.mousePosition;

            if (clearAfterClick && Input.GetMouseButtonUp(0))
            {
                ClearCursor();
            }
        }
    }

    /// <summary>
    /// Snaps an icon on cursor position.
    /// </summary>
    /// <param name="icon"></param>
    /// <param name="clearAfterClick"></param>
    public void SetCursor(Sprite icon, bool clearAfterClick = false)
    {
        isActivated = true;
        this.icon.sprite = icon;
        this.icon.gameObject.SetActive(true);
        this.clearAfterClick = clearAfterClick;
    }

    /// <summary>
    /// Clears the cursor.
    /// </summary>
    public void ClearCursor()
    {
        isActivated = false;
        icon.sprite = null;
        icon.gameObject.SetActive(false);
    }
}
