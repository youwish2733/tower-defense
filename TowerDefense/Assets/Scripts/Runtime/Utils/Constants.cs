using UnityEngine;

public static class Constants
{
    public static class Game
    {
        public const int ENEMY_VALUE = 5;
        public const int UPGRADE_COST = 25;
    }

    public static class Colors
    {
        public const string FREE_TILE = "#57D043";
        public const string OCCUPIED_TILE = "#EA2A20";
    }
}
